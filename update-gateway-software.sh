#!/bin/sh

# Author: Aravinth Panchadcharam

# Set config parameters here
SETUP_DIR=/home/yetu-admin
USER="yetu-admin"
GROUP="yetu-admin"
CONFIG_FILE=global.sh
SETUP_DAEMON="gateway-init"
DIST_URL="https://us-east.manta.joyent.com/yetu/public/downloads/gateway-setup.zip"


# Stop the daemon so we can update it
echo "Stopping Gateway Setup"
/etc/init.d/$SETUP_DAEMON stop

if [ ! -d $SETUP_DIR ]; then
	echo "ERROR: Gateway Setup directory does not exist"
	exit 1
fi


# Change to Gateway Setup directory
cd $SETUP_DIR


# Copy Global Config File
if [ -e common/scripts/$CONFIG_FILE ]; then
    echo "Saving global.sh"
    cp common/scripts/$CONFIG_FILE /tmp/$CONFIG_FILE
fi


#removing temp files
if [ -d common.old ]; then
    echo "Removing old temp files"
    rm -rf common.old
fi


#removing downloaded files
if [ -e gateway-setup.zip ]; then
    echo "Removing old downloaded file"
    rm gateway-setup.zip
fi


# Make saftey copy
if [ -d common ]; then
	echo "Dumping current Gateway Setup"
	mv common common.old
fi


# Download new Gateway Setup
echo "Downloading latest Gateway Setup"
wget -q $DIST_URL


# Check the file size
if [ ! -s gateway-setup.zip ]; then
	echo "ERROR: Downloaded file has size of zero or does not exist"
	exit 2
fi


# Unzip the distribution
echo "Unzipping Distribution"
unzip -q gateway-setup.zip


# Copy the global.sh back
if [ -e /tmp/$CONFIG_FILE ]; then
	echo "Restoring global.sh"
	cp /tmp/$CONFIG_FILE common/scripts/$CONFIG_FILE
fi


# Setting Permission
echo "Setting necessary permissions on files and directories"
chown -R $USER:$GROUP $SETUP_DIR