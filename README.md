# Gateway-Setup-Scripts #
These scripts allow the initial setup of the gateway.
Please see https://yetu-dev.atlassian.net/wiki/display/TGI/Gateway+Setup for further information.

Installation instructions:

* move common directory to /home/yetu-admin/

* move etc-init/gateway-init script to /etc/init.d/ and do update-rc.d gateway-init defaults
* ensure that all scripts belong to user yetu-admin