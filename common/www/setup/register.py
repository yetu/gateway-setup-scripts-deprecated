import subprocess, sys, os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
includesdir = os.path.dirname(currentdir)+"/includes"
sys.path.insert(0,includesdir)

import helpers

data = sys.stdin.read()
succ,auth_code=helpers.extractJson(data, "auth_code")
succ,redirect_uri=helpers.extractJson(data, "redirect_uri")
succ,email=helpers.extractJson(data, "email")
subprocess.Popen(["bash", "../../scripts/webserver/setup/register.sh", auth_code, redirect_uri, email])
