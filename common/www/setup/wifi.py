import subprocess, sys, os, inspect
from cgi import escape

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
includesdir = os.path.dirname(currentdir)+"/includes"
sys.path.insert(0,includesdir)

import helpers

data = sys.stdin.read()
succ,wifi_ssid=helpers.extractJson(data, "ssid")
succ,wifi_passwd=helpers.extractJson(data, "passwd")
succ,wifi_type=helpers.extractJson(data, "type")
subprocess.Popen(["bash", "../../scripts/webserver/setup/wifi.sh", wifi_ssid, wifi_passwd, wifi_type.upper()])
