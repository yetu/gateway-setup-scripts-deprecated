#!/usr/bin/python

import json
import sys

def extractJson(jsonString, jsonKey):
	# one or two arguments are empty
	if (len(jsonString) == 0 or len(jsonKey)) == 0:
		return 0, ""

	try:
		jsonObj=json.loads(jsonString)
	except ValueError, e:
		# not a valid json object
		return 0, ""

	if jsonKey in jsonObj:
		return 1, jsonObj[jsonKey]
	else:
		# key not found
		return 0, ""
