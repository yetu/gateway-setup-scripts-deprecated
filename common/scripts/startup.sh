#!/bin/bash

# This script is responsible for booting up the system in normal or setup mode. 
# authors: Pooran Patel, Sebastian Garn

STARTUP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$STARTUP_DIR/helpers.sh"

resetLog
log "STARTUP SCRIPT CALLED"

wifi_passwd=""
wifi_result="0"
online_status="0"

# starts yetu gateway
startGatewaySoftware () {
	log "Starting gateway software stack"
	logGatewayState $GATEWAY_SOFTWARE_STARTING
	res=$(sudo /etc/init.d/yetu start)
}

startSetupMode () {
	log "Starting gateway setup mode"
	switchToAccessPointMode
}

resetGatewayStateLog
logGatewayState $INITIALIZING_GATEWAY

# start lighttpd
lighttpd_started=$(pidof lighttpd)
if [ "${#lighttpd_started}" == "0" ]; then
	sudo /etc/init.d/lighttpd start
fi

# try to use normal wifi mode
wifi_state=""
switchToNormalWifiMode &> /dev/null

# extract gateway id
smarthome_cfg_file_exists=$(fileExists $SMARTHOME_CFG)
if [ "$smarthome_cfg_file_exists" == "0" ]; then
	echo "No smarthome.cfg found"
	exit
fi

gw_id=$(getStoredGatewayId)
log "Gateway id is: $gw_id"

# get online status
is_online=$(isOnline)
log "Is online: $is_online"

if [ "$gw_id" == "" ]; then
	logGatewayState $READY_FOR_SETUP
	startSetupMode
else
	if [ "$is_online" == "0" ] && [ "$wifi_state" == "$WIFI_OK" ]; then
		logGatewayState $NO_INTERNET_CONNECTION
		startGatewaySoftware
	elif [ "$is_online" == "0" ] && [ "$wifi_state" != "$WIFI_OK" ]; then
		startSetupMode
	else
		startGatewaySoftware
	fi
fi

