#!/bin/bash

HELPERS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$HELPERS_DIR/global.sh"

wifi_state=0

function resetLog() {
	echo "" > $LOG_FILE
}

function log() {
	time=$(date +"%Y-%m-%d %H:%M:%S,%3N")
	echo "$time: $1" >> $LOG_FILE
}

sendHTTPResponse () {
	echo "Content-Type: text/html"
	echo "Status: $1"
	echo ""
	echo "$2"
}

# extracts the value of an entry
#  param String file: path to file
#  param String key: entry you are searching for (e.g. "myOption=")
#  return the value of the entry or an empty string 
getFileValue () {
    searchFor=$2
    tmp=$(cat $1 | grep ^$searchFor)
    replaceWith=""
    echo ${tmp/$searchFor/$replaceWith}
}

# checks whether a file exists or not
#  param String file
#  return 0 if file was not found, 1 if it exists 
fileExists () {
    if [ ! -f "$1" ]; then
            echo 0
    else
            echo 1
	fi
}

# Searches for the smarthome.cfg and extracts the gateway
function getStoredGatewayId () {
	smarthome_cfg_file_exists=$(fileExists $SMARTHOME_CFG)
	if [ "$smarthome_cfg_file_exists" == "1" ]; then
		echo "$(getFileValue $SMARTHOME_CFG 'yetubackend:gatewayid=')"
	else
		logGatewayState $CFG_FILE_MISSING
	fi
}

# Checks the current state of the wifi
#  returns WIFI_OK, WIFI_NOT_FOUND, WIFI_ERROR, WIFI_MISSING_CONF, WIFI_BLOCKED or WIFI_AUTH_FAILED
function getWifiConnectionState() {
	log "Get wifi connection state"

	tries=20
	delay=0.5

	curr_try=0
	state=""
	while [ $curr_try -lt $tries ]
	do
        	last_state=$(sudo wpa_cli status | grep "wpa_state")

		log "$last_state"

	        # stop if we have a valid connection
	        if [ "$last_state" == "wpa_state=COMPLETED" ]; then
        	        state="COMPLETE"
                	curr_try=$tries
	        # sending passwd
        	elif [ "$last_state" == "wpa_state=4WAY_HANDSHAKE" ]; then
                	state="AUTH"
	        # wifi network was found
	        elif [ "$last_state" == "wpa_state=ASSOCIATING" ] && [ "$state" != "AUTH" ]; then
        	        state="BLOCKED"
	        # scanning for network - only store this value if there previously was no handshake nor disconnect
      	  	elif [ "$last_state" == "wpa_state=SCANNING" ] && [ "$state" != "AUTH" ] && [ "$state" != "BLOCKED" ]; then
                	state="SCANNING"
	        fi

	        curr_try=$((curr_try+1))
        	sleep $delay
	done

	if [ "$state" == "COMPLETE" ]; then
		log "Wifi state complete - wait for ip"

	        # we are expecting an ip address after 20 seconds
        	i=0
        	while [ $i -lt 20 ]
        	do
                	wifi_state=$(ip addr show $WIFI_INTERFACE)

                	if [ $(containsSubstring "$wifi_state" "inet") == "1" ]; then
				log "Wifi success $wifi_state"
                        	echo "$WIFI_OK"
                        	return
                	fi
                	sleep 1
                	i=$[$i+1]
        	done

		# did not get ip
		echo "$WIFI_MISSING_CONF"
	elif [ "$state" == "BLOCKED" ]; then
	        echo "$WIFI_BLOCKED"
	elif [ "$state" == "AUTH" ]; then
	        echo "$WIFI_AUTH_FAILED"
	elif [ "$state" == "SCANNING" ]; then
	        echo "$WIFI_NOT_FOUND"
	else
	        echo "$WIFI_ERROR"
	fi
}

# Disables and enables the wifi interface
#  param new_mode String: the interface configuration (normalmode or apmode) to use after reset
#  param run_in_background int: 1 true, 0 false
function resetWifiInterface() {
	log "Resetting wifi interface $WIFI_INTERFACE ($1)"
	sudo ifdown $WIFI_INTERFACE
	sleep 1

	if [ "$2" != "" ] && [ "$2" == "1" ]; then
		log "Enabling wifi interface $WIFI_INTERFACE non-blocking"
		sudo ifup $WIFI_INTERFACE=$1 &
	else
		log "Enabling wifi interface $WIFI_INTERFACE (blocking)"
		sudo ifup $WIFI_INTERFACE=$1
	fi
}


# Disables and enables the ethernet interface if cable is plugged in
#  param new_mode String: the interface configuration (normalmode or apmode) to use after reset
#  param run_in_background int: 1 true, 0 false
function resetEthernetInterface() {
	log "Resetting ethernet interface $ETH_INTERFACE"
        sudo ifdown $ETH_INTERFACE

	eth_state=""
	try=0
	while [ $try -le 5 ];
	do
		log "Checking state of $ETH_INTERFACE"
		eth_state=$(tail /sys/class/net/eth0/carrier)

		if [ "$eth_state" == "" ]; then
			log "$ETH_INTERFACE not ready"
			try=$((try+1))
			sleep 1
		else
			try=6
		fi
	done

	if [ ${#eth_state} != 0 ] && [ "$eth_state" == "1" ]; then
	        sleep 1

        	if [ "$1" != "" ]  && [ "$1" == "1" ]; then
			log "Enabling eth interface non-blocking"
                	sudo ifup $ETH_INTERFACE &
	        else
			log "Enabling eth interface (blocking)"
        	        sudo ifup $ETH_INTERFACE
	        fi
	else
		log "Abort reset - no cable plugged in"
	fi
}

# Extracts wifi information from file and tries to connect to the wifi network
#  returns 0 if wifi connection did not work
function switchToNormalWifiMode() {
	log "Switching to normal wifi"

        # no config file
        if [ "$(fileExists $WIFI_CFG)" == "0" ]; then
		wifi_state="WIFI_ERROR"
                return
        fi

        # stop dhcp server
        dhcpserver_state=$($DHCPSERVER_PATH status)
        dhcpserver_running=$(containsSubstring "$dhcpserver_state" "is running")

        if [ $dhcpserver_running == 1 ]; then
                sudo $DHCPSERVER_PATH stop
        fi

        # stop hostapd if it is running
        hostapd_state=$($HOSTAPD_PATH status)
        hostapd_running=$(containsSubstring "$hostapd_state" "is running")

        if [ $hostapd_running == 1 ]; then
                sudo $HOSTAPD_PATH stop
        fi

        supplicant_running=$(pidof wpa_supplicant)
	wifi_connected_ssid=$(iwgetid -r)

        # start wpa supplicant if it is not running
        if [ ${#supplicant_running} != 0 ]; then
                sudo wpa_cli terminate
        fi

	log "Starting wpa supplicant"
	sudo wpa_supplicant -i $WIFI_INTERFACE -c $WIFI_CFG -B

	resetWifiInterface "normalmode" 0

        wifi_state=$(getWifiConnectionState)
	log "Wifi state: ${STATES[$wifi_state]}"

	log "Switch to normal mode DONE"

}

function switchToAccessPointMode() {
	log "Switching to ap mode"

        # stop wpa_supplicant
        wpa_supplicant_running=$(pidof wpa_supplicant)
        if [ ${#wpa_supplicant_running} != 0 ]; then
		log "Killing wpa_supplicant"
                sudo wpa_cli terminate
        fi

        hostapd_state=$($HOSTAPD_PATH status)
        hostapd_running=$(containsSubstring "$hostapd_state" "is running")

        if [ $hostapd_running == 1 ]; then
		log "hostapd already running. Stopping it."
		sudo $HOSTAPD_PATH stop
	fi

	resetEthernetInterface 0

	sleep 2

	resetWifiInterface "apmode" 0

	log "Starting hostapd"
        sudo $HOSTAPD_PATH start


        # start dhcp server
        dhcpserver_state=$($DHCPSERVER_PATH status)
        dhcpserver_running=$(containsSubstring "$dhcpserver_state" "is running")

        if [ $dhcpserver_running == 1 ]; then
		log "Stopping dhcp server"
                sudo $DHCPSERVER_PATH stop
        fi

	log "Starting dhcp server"
        sudo $DHCPSERVER_PATH start

	# check state
        dhcpserver_state=$($DHCPSERVER_PATH status)
        dhcpserver_running=$(containsSubstring "$dhcpserver_state" "is running")
	if [ $dhcpserver_running != 1 ]; then
		log "Switching to access point mode failed. Retry ..."
		switchToAccessPointMode
	else
		log "Switch to access point mode DONE"
	fi
}

# checks whether the system reaches a remote location or not
#  returns 1 if connect was successful, 0 if not
isOnline () {
	response=$(curl $URL_CHECK_CONNECTIVITY --cacert $CERT_FILE)
	if [ "${#response}" -gt 0 ]; then
		echo 1
	else
		echo 0
	fi
}

# extracts a header value
#  param String header
#  param String header_key: the key you are looking for
# return empty string if no value was found or value is empty
getHeaderValue () {
	search_for="$2"
	value=$(echo "$1" | grep "^$search_for:")

	remain=$(expr ${#value} - ${#search_for} - 1)
	result=${value:$(expr ${#search_for} + 1):$remain}

	if [ "${#search_for}" == "0" ]; then
        	echo ""
	fi

	# remove leading space
	if [ "${result:0:1}" == " " ]; then
        	result=${result:1:(${#result}-1)}
	fi

	echo "$result";
}

# checks whether a given string contains a substring
#  param String given_string
#  param String search_for
# return 1 if found
containsSubstring () {
	result=$(echo "$1" | grep "$2")

        if [ ${#result} -gt 0 ]; then
                echo 1
        else
                echo 0
        fi
}

resetGatewayStateLog () {
	echo "" > $GATEWAY_STATE_LOG
}

logGatewayState () {
	echo "${STATES[$1]}" >> "$GATEWAY_STATE_LOG"
}

# Adds a new entry in the smarthome.cfg. If entry already exists it gets replaced
#  param String config key (e.g. yetubackend:gatewayid)
#  param String value
addConfigurationProperty () {
	smarthome_cfg_file_exists=$(fileExists $SMARTHOME_CFG)
	key="$1="
	line=$(grep "^$key" "$SMARTHOME_CFG")
	newline="$key$2"

	if [ "${#line}" != "0" ]; then
        	# replace line
        	sed s/^$key.*$/$newline/ "$SMARTHOME_CFG" > "$SMARTHOME_CFG.NEW" ; mv "$SMARTHOME_CFG.NEW" "$SMARTHOME_CFG"
	else
        	# append
	        echo "$newline" >> "$SMARTHOME_CFG"
	fi
}
