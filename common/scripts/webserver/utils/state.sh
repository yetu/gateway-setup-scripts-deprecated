#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/../../helpers.sh

# Reads the current state of the gateway
# authors: Pooran Patel, Sebastian Garn

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
state=$(tail -n 1 "$DIR/../../../storage/state.info")
sendHTTPResponse 200 "$state"
