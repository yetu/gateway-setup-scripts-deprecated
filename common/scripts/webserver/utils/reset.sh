#!/bin/bash

# resets the gateway id in setup script. This script is just for testing purposes and will be removed before the gw is shipped to end user.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$DIR/../../helpers.sh"

log "RESET ENDPOINT CALLED"

gw_id="$(getStoredGatewayId)"
if [ "$gw_id" == "" ]; then
	log "No gateway_id found"
        sendHTTPResponse 404 ""
        exit
fi

# log "Sending request to backend"

# response=$(curl -i --request POST "$BACKEND_URL:$BACKEND_PORT/management/gateway/reset" --data "{\"gateway_id\":\"$gw_id\"}" -H "Content-type: application/json")
# response_code=$(echo "$response" | grep "200 OK")

# if [ ${#response_code} == 0 ]; then
# 	sendHTTPResponse 503 "Unable to reset state. Please check network connection and try again"
# 	exit
# fi

log "Resetting now"
logGatewayState $RESET_IN_PROGRESS

# stop gateway software if needed

log "Stopping gateway software stack"
yetu_started=$(pidof java)
if [ "${#yetu_started}" != "0" ]; then
	sudo /etc/init.d/yetu stop
fi

# remove gateway id
search_for="yetubackend:gatewayid=$gw_id"
res=$(sed "s/$search_for//g" "$SMARTHOME_CFG")
$req=$(echo "$res" > "$SMARTHOME_CFG")

#remove zwave files
rm "$SMARTHOME_CFG_DIR/zwave_devices.xml"
rm "$SMARTHOME_CFG_DIR/zwave_nodes.xml"
rm "$SMARTHOME_CFG_DIR/../etc/mapdb/storage.mapdb"
rm "$SMARTHOME_CFG_DIR/../etc/mapdb/storage.mapdb.p"
rm "$SMARTHOME_CFG_DIR/../etc/mapdb/storage.mapdb.t"

sendHTTPResponse 200 ""

exec >&-
exec 2>&-

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
$DIR/../../startup.sh
