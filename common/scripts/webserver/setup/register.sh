#!/bin/bash

# This script sends an access token to the backend and stores the received gw id
# authors: Pooran Patel, Sebastian Garn
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$DIR/../../helpers.sh"

log "REGISTER ENDPOINT CALLED: auth_code=$1 redirect_uri=$2 email=$3"

json_error=0;
json_res="";

parseJson () {
    result=$(python "$DIR/../json_extract.py" "$1" "$2")
    json_error=$?
    json_res=$result
}


# do we expose the endpoint - check preconditions
gw_id="$(getStoredGatewayId)"
if [ "$gw_id" != "" ]; then
	sendHTTPResponse 404 ""
	exit
fi

auth_code=$1

if [ "$auth_code" == "" ]; then
	sendHTTPResponse 400 ""
	exit
fi

redirect_uri=$2

if [ "$redirect_uri" == "" ]; then
        sendHTTPResponse 400 ""
        exit
fi

# return 200 because we now have to wait
sendHTTPResponse 200 ""

# interrupt echo so the browser gets a response immediately
exec >&-
exec 2>&-

log "Auth in progress"
logGatewayState $AUTH_IN_PROGRESS

# wifi connect
log "Switching to wifi to validate auth code"
wifi_state=""
switchToNormalWifiMode &> /dev/null

# check internet connection
is_online=$(isOnline)
log "Online: $is_online"
if [ "$is_online" == "0" ]; then
	log "No internet connection"
	logGatewayState $NO_INTERNET_CONNECTION
	switchToAccessPointMode
	exit
fi

# send gateway id and auth token to backend
log "Sending request"

response=$(curl -i --request POST "$BACKEND_URL:$BACKEND_PORT/management/gateway/register" -H "Authorization:OAuth $auth_code" -H "Redirect-Uri:$redirect_uri" --cacert $CERT_FILE)
response_code=$(echo "$response" | grep "200 OK")

log "Received response: $response"

if [ ${#response_code} == 0 ]; then
	log "Invalid response code"
	logGatewayState $AUTH_FAILED
	switchToAccessPointMode
	exit
fi

body_started=0

# read http response line by line and echo content whenever empty line (content length smaller 1) was detected
body=$(printf "%s\n" "$response" | while IFS= read -r line
do
       	if [ "$body_started" == "1" ]; then
               	echo "$line"
        elif [ ${#line} -le 1 ]; then
       	        body_started=1
	fi
done)

log "Extracting response body"

# extract gateway id
parseJson "$body" "gateway_id"
if [ "$json_error" != "0" ]; then
	log "No gateway_id found in response"
	logGatewayState $GATEWAY_SETUP_FAILED
	switchToAccessPointMode
       	exit
fi

gateway_id=$json_res

# extract cumulocity credentials
parseJson "$body" "management_user_id"
if [ "$json_error" != "0" ]; then
	log "No management_user_id found in response"
	json_res=""
fi

management_user_id=$json_res

parseJson "$body" "management_password"
if [ "$json_error" != "0" ]; then
	log "No management_password found in response"
	json_res=""
fi

management_password=$json_res

parseJson "$body" "external_id"
if [ "$json_error" != "0" ]; then
	log "No external_id found in response"
       	json_res=""
fi

external_id=$json_res

if [ "$gateway_id" == "" ]; then
	log "Gateway id is empty. Abort setup"
	logGatewayState $GATEWAY_SETUP_FAILED
	switchToAccessPointMode
        exit
fi

# store the gateway id
addConfigurationProperty "yetubackend:gatewayid" "$gateway_id"
addConfigurationProperty "yetumanagementcumulocity:login" "$management_user_id"
addConfigurationProperty "yetumanagementcumulocity:password" "$management_password"
addConfigurationProperty "yetumanagementcumulocity:externaldeviceid" "$external_id"


# generate public/private key for tv login
rm "$KEYSTORE_PATH"

# generate key pair
ssh-keygen -t rsa -f "$KEYSTORE_PATH" -P ""

# extract and public key and put it into single line format
#publickey=$(ssh-keygen -f "$KEYSTORE_PATH" -y)
publickey=$(cat "$KEYSTORE_PATH.pub")

echo "Public key:"
echo $publickey

# switch back to ap so the setup can get the latest state
switchToAccessPointMode

log "Auth successful. Saving gateway id"
logGatewayState $AUTH_OK
