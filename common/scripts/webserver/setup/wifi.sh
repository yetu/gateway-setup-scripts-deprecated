# TODO receives a post request containing wifi ssid and passwd checks whether it can connect to that wifi network
#  if yes stores information
#  if no returns error code
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$DIR/../../helpers.sh"

log "WIFI SETUP ENDPOINT CALLED: ssid=$1 passwd=$2 type=$3"

function createWPA2Configuration() {
	rm $WIFI_CFG
	printf "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1\n\nnetwork={\n" >> $WIFI_CFG
	printf "\tssid=\"$1\"\n" >> $WIFI_CFG
	printf "\tpsk=\"$2\"\n" >> $WIFI_CFG
	printf "\tproto=WPA2\n" >> $WIFI_CFG
	printf "\tkey_mgmt=WPA-PSK\n" >> $WIFI_CFG
	printf "}\n" >> $WIFI_CFG
}

function createWPAConfiguration() {
        rm $WIFI_CFG
        printf "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1\n\nnetwork={\n" >> $WIFI_CFG
        printf "\tssid=\"$1\"\n" >> $WIFI_CFG
        printf "\tpsk=\"$2\"\n" >> $WIFI_CFG
        printf "\tproto=WPA\n" >> $WIFI_CFG
        printf "\tkey_mgmt=WPA-PSK\n" >> $WIFI_CFG
        printf "}\n" >> $WIFI_CFG
}

function createNoEncryptionConfiguration() {
        rm $WIFI_CFG
        printf "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1\n\nnetwork={\n" >> $WIFI_CFG
        printf "\tssid=\"$1\"\n" >> $WIFI_CFG
        printf "\tkey_mgmt=NONE\n" >> $WIFI_CFG
        printf "}\n" >> $WIFI_CFG
}

function createWEPConfiguration() {
	rm $WIFI_CFG
        printf "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1\n\nnetwork={\n" >> $WIFI_CFG
        printf "\tssid=\"$1\"\n" >> $WIFI_CFG
        printf "\twep_key0=\"$2\"\n" >> $WIFI_CFG
        printf "\tkey_mgmt=NONE\n" >> $WIFI_CFG
        printf "}\n" >> $WIFI_CFG
}

wifi_ssid="$1"
wifi_passwd="$2"
wifi_type="$3"

if [ "$wifi_ssid" == "" ]; then
        sendHTTPResponse 400 ""
        exit
fi

# check if type is supported
if [ "$wifi_type" != "WPA2" ] && [ "$wifi_type" != "WPA" ] && [ "$wifi_type" != "WEP" ] && [ "$wifi_type" != "" ]; then
	log "Not supported"
	sendHTTPResponse 500 ""
	exit
fi

# expecting passwd
if [ "$wifi_type" == "WPA2" ] || [ "$wifi_type" == "WPA" ] || [ "$wifi_type" == "WEP" ]; then
	if [ "$wifi_passwd" == "" ]; then
		log "No password given"
		sendHTTPResponse 400 ""
		exit
	fi
elif [ "$wifi_type" == "" ] && [ "$wifi_passwd" != "" ]; then
	log "Password not necessary"
	sendHTTPResponse 400 ""
	exit
fi

# backup wifi cfg
WIFI_CFG_BACKUP="$WIFI_CFG.BACKUP"
mv $WIFI_CFG $WIFI_CFG_BACKUP

# create new wifi cfg file
if [ "$wifi_type" == "WPA2" ]; then
	createWPA2Configuration "$wifi_ssid" "$wifi_passwd"
elif [ "$wifi_type" == "WPA" ]; then
        createWPAConfiguration "$wifi_ssid" "$wifi_passwd"
elif [ "$wifi_type" == "WEP" ]; then
        createWEPConfiguration "$wifi_ssid" "$wifi_passwd"
elif [ "$wifi_type" == "" ]; then
	createNoEncryptionConfiguration "$wifi_ssid"
fi

sendHTTPResponse 200 ""

exec >&-
exec 2>&-

log "Checking wifi information"
logGatewayState $WIFI_CHECKING
wifi_state=0
switchToNormalWifiMode &> /dev/null

if [ "$wifi_state" == "$WIFI_OK" ]; then
	log "Wifi fine - saving new config"
	rm $WIFI_CFG_BACKUP
else
	log "Wifi failed: $wifi_state Restoring old configuration"
	mv $WIFI_CFG_BACKUP $WIFI_CFG
fi

switchToAccessPointMode

logGatewayState $wifi_state
