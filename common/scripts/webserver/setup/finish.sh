#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/../../helpers.sh

# This script allows the set up to finish the setup process
# authors: Pooran Patel, Sebastian Garn

log "FINISH SETUP ENDPOINT CALLED"

# do we expose the endpoint - check preconditions
gw_id="$(getStoredGatewayId)"
if [ "$gw_id" == "" ]; then
        sendHTTPResponse 404 ""
        exit
fi

yetu_started=$(pidof java)
if [ "${#yetu_started}" != "0" ]; then
	sendHTTPResponse 404 ""
	exit
fi

sendHTTPResponse 200 ""

exec >&-
exec 2>&-

log "Restarting..."

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
$DIR/../../startup.sh
