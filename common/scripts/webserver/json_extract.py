#!/usr/bin/python

import json
import sys

# Usage: python json_extract.py JSONSTRING KEYTOSEARCHFOR
# return value of given key or exits with code 1 parameters are invalid or 2 if key not found

# requires two parameters
if len(sys.argv) < 3:
	sys.exit(1)

# one or two arguments are empty
if (len(sys.argv[1]) == 0 or len(sys.argv[2])) == 0:
	sys.exit(1)

try:
	jsonObj=json.loads(sys.argv[1])
except ValueError, e:
	# not a valid json object
	sys.exit(1)

if sys.argv[2] in jsonObj:
	print jsonObj[sys.argv[2]]
else:
	# key not found
	sys.exit(2)
